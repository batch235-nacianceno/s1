// mocha framework; chai has the methods
// import assert method from chai
const { assert } = require("chai");
// unit to be tested
const { newUser, user } = require("../index.js");

// describe groups together the tests
// if there are multiple objects in the index.js - you can separate each object in a describe
// anonymous function - it (individual test) - check if the element is really an object

// messages should be descriptive - it helps you understand what is wrong or what is being tested

// typeof returns a string that's why "object" / "string"

describe("Test newUser object", () => {
    it ("Assert newUser type is an object", () => {
        assert.equal(typeof(newUser), "object");
    })

    it ("Assert newUser email is type string", () => {
        assert.equal(typeof(newUser.email),"string");
    })

    it ("Assert newUser email is not undefined", () => {
        assert.notEqual(typeof(newUser.email),"undefined");
    })
    it ("Assert newUser password is type string", () => {
        assert.equal(typeof(newUser.password),"string");
    })
    // check minimun number of characters
    it ("Assert newUser password is at least 16 characters long", () => {
        assert.isAtLeast(newUser.password.length, 16);
    })
});

// Activity
//to test individual test suite run : npm test -- -f user/newUser




describe("Test user object", () => {
    it ("Assert that the user firtName type is a string", () => {
        assert.equal(typeof(user.firstName), "string");
    })

    it ("Assert that the user lastName type is a string", () => {
        assert.equal(typeof(user.lastName),"string");
    })

    it ("Assert that the user firstName is not undefined", () => {
        assert.notEqual(typeof(user.firstName),"undefined");
    })

    it ("Assert that the user lastName is not undefined", () => {
        assert.notEqual(typeof(user.lastName),"undefined");
    })

    it ("Assert that the user age is at least 18", () => {
        assert.isAtLeast(user.age, 18);
    })

    it ("Assert that the user contact number is a string", () => {
        assert.equal(typeof(user.contactNumber),"string");
    })

    it ("Assert that the user batch number type is a number", () => {
        assert.equal(typeof(user.batchNumber),"number");
    })

    it ("Assert that the user batch number is not undefined", () => {
        assert.notEqual(typeof(user.batchNumber),"undefined");
    })

    it ("Assert that the user password is at least 16 characters", () => {
        assert.isAtLeast(user.password.length , 16);
    })

    
});


